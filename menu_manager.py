from telebot import types

import settings
from main import bot

bot = settings.bot
db = settings.db

bot = settings.bot

def show_main_menu(chat_id):
    login_flag = not db.exists(chat_id) or db.hget(chat_id, "Logged_in").decode("utf-8") == '0'

    keyboard = types.InlineKeyboardMarkup()
    key_register = types.InlineKeyboardButton(text='Register', callback_data='register')

    if login_flag:
        key_login = types.InlineKeyboardButton(text='Log in', callback_data='login')
    else:
        key_login = types.InlineKeyboardButton(text='Log out', callback_data='logout')

    key_get_all_data = types.InlineKeyboardButton(text="Get all data", callback_data='show_all_data')
    key_delete_all_data = types.InlineKeyboardButton(text="Delete all data", callback_data='delete_all_data')

    key_write_text = types.InlineKeyboardButton(text="Write text", callback_data='write_text')
    key_write_file = types.InlineKeyboardButton(text="Write file", callback_data='write_file')

    keyboard.add(key_register)
    keyboard.add(key_login)
    keyboard.add(key_get_all_data)
    keyboard.add(key_delete_all_data)
    keyboard.add(key_write_text)
    keyboard.add(key_write_file)
    bot.send_message(chat_id=chat_id, text="What do you want?", reply_markup=keyboard)

