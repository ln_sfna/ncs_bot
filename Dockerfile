FROM python:3.8
WORKDIR /app
COPY . .
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip3 install -r requirements.txt
RUN chmod 777 -R .
EXPOSE 8080 8080
ENTRYPOINT ["python", "main.py"]
