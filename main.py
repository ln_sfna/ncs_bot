import asyncio

import menu_manager
import server_functions
import settings

db = settings.db
bot = settings.bot

STATE_USER_LOGIN = "state_user_login"
STATE_USER_LOGOUT = "state_user_logout"
STATE_USER_REGISTER = "state_user_register"
STATE_USER_WRITE_TEXT = "state_user_write_text"
STATE_USER_WRITE_FILE = "state_user_write_file"
STATE_USER_DELETE_DATA = "state_user_delete_data"


# Database organisation:
# hash structure name: chat_id
# Subscribed: can be 0, 1 or 2. 0 if not subscribed, 1 if subscribed, 2 if subscribed to some projects
# Previous_state: can be 0 or some callback text, used to process messages sent by user correctly
# Subscribed_projects: can be 0 or some project names


@bot.message_handler(commands=['start'])
def start(message):
    db.hset(message.chat.id, "Logged_in", '0')
    db.hset(message.chat.id, "Previous_state", '0')

    bot.send_message(chat_id=message.chat.id,
                     text="Hello, use command /menu to continue")


@bot.message_handler(commands=['menu'])
def menu(message):
    menu_manager.show_main_menu(message.chat.id)


@bot.callback_query_handler(func=lambda call: True)
def callback_getter(call):
    if call.data == "register":
        db.hset(call.message.chat.id, "Previous_state", STATE_USER_REGISTER)
        bot.send_message(chat_id=call.message.chat.id, text="Send authentication data in format:\nlogin\npassword")

    elif call.data == "login":
        db.hset(call.message.chat.id, "Previous_state", STATE_USER_LOGIN)
        bot.send_message(chat_id=call.message.chat.id, text="Send authentication data in format:\nlogin\npassword")

    elif call.data == "logout":
        server_functions.logout_user(call.message.chat.id)

    elif call.data == "show_all_data":
        server_functions.get_all_data(call.message.chat.id)

    elif call.data == "delete_all_data":
        bot.send_message(chat_id=call.message.chat.id, text="Please write 'delete all data' to confirm operation")
        db.hset(call.message.chat.id, "Previous_state", STATE_USER_DELETE_DATA)

    elif call.data == "write_text":
        db.hset(call.message.chat.id, "Previous_state", STATE_USER_WRITE_TEXT)
        bot.send_message(chat_id=call.message.chat.id, text="Send text to write:")

    elif call.data == "write_file":
        db.hset(call.message.chat.id, "Previous_state", STATE_USER_WRITE_FILE)
        bot.send_message(chat_id=call.message.chat.id, text="Send file to write:")

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    previous_state = db.hget(message.chat.id, "Previous_state").decode("utf-8")
    if previous_state == STATE_USER_REGISTER:
        db.hset(message.chat.id, "Previous_state", '0')
        server_functions.register_user(message.chat.id, message.text)
    elif previous_state == STATE_USER_LOGIN:
        db.hset(message.chat.id, "Previous_state", '0')
        server_functions.login_user(message.chat.id, message.text)
    elif previous_state == STATE_USER_WRITE_TEXT:
        db.hset(message.chat.id, "Previous_state", '0')
        server_functions.write_data(message.chat.id, "text", message.text)
    elif previous_state == STATE_USER_DELETE_DATA:
        db.hset(message.chat.id, "Previous_state", '0')
        if message.text == "delete all data":
            server_functions.delete_all_data(message.chat.id)
        else:
            bot.send_message(chat_id=message.chat.id, text="Operation was not applied")
            menu_manager.show_main_menu(message.chat.id)


@bot.message_handler(content_types=['document', 'image'])
def get_document_messages(message):
    previous_state = db.hget(message.chat.id, "Previous_state").decode("utf-8")
    if previous_state == STATE_USER_WRITE_FILE:
        db.hset(message.chat.id, "Previous_state", '0')
        server_functions.write_data(message.chat.id, 'file', message.document)


if __name__ == '__main__':
    print("Bot started", flush=True)
    bot.polling()

    pass
