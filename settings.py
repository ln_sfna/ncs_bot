import os

import redis
import telebot

token = os.getenv('TOKEN')
bot = telebot.TeleBot(token)
db = redis.StrictRedis(host=os.getenv("REDIS_HOST"), port=int(os.getenv("REDIS_PORT")), db=int(os.getenv("REDIS_DATABASE")))
