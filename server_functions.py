import ast
import hashlib
import json
import os

import requests

import menu_manager
import settings

db = settings.db
bot = settings.bot

shutdown = False
SERVER_HOST = os.getenv("SERVER_HOST")
SERVER_PORT = os.getenv("SERVER_PORT")


def __encrypt_password(password):
    hasher = hashlib.md5()
    hasher.update(password.encode())
    return hasher.digest()


def get_all_data(chat_id):
    url = f"http://{SERVER_HOST}:{SERVER_PORT}/data/show-all-data?id={chat_id}"
    r = requests.get(url, headers={"Chat_id": str(chat_id)})
    response = json.loads(r.json())

    if response.get("Status code") == 200:
        content = json.loads(response.get("Details"))
        text_data = []
        file_data = []
        for dict in content.values():
            data = []
            if dict["type"] == "text":
                data.append(dict["time"])
                data.append(str(dict["data"]))
                text_data.append(' '.join(data))
            elif dict["type"] == "file":
                file_data.append(dict)

        bot.send_message(chat_id=chat_id, text="Your data:")

        if len(text_data) > 0:
            bot.send_message(chat_id=chat_id, text="\n".join(text_data))
        if len(file_data) > 0:
            for dict in file_data:
                file = dict["data"]
                file = ast.literal_eval(file)
                bot.send_document(chat_id=chat_id,document=file["file_id"], caption=dict["time"])
    else:
        bot.send_message(chat_id=chat_id, text=response.get("Details"))

    menu_manager.show_main_menu(chat_id)


def delete_all_data(chat_id):
    url = f"http://{SERVER_HOST}:{SERVER_PORT}/data/delete-all-data?id={chat_id}"
    r = requests.get(url, headers={"Chat_id": str(chat_id)})
    response = json.loads(r.json())

    bot.send_message(chat_id=chat_id, text=response.get("Details"))
    menu_manager.show_main_menu(chat_id)


def register_user(chat_id, data):
    login = ''
    password = ''
    try:
        login, password = data.split("\n")
    except ValueError:
        bot.send_message(chat_id=chat_id, text="Invalid data format")
    password = __encrypt_password(password)

    url = f"http://{SERVER_HOST}:{SERVER_PORT}/auth/register?id={chat_id}&login={login}&pswd={password}"
    r = requests.get(url, headers={"Chat_id": str(chat_id)})
    r = r.json()
    response = json.loads(r)

    bot.send_message(chat_id=chat_id, text=response.get("Details"))
    menu_manager.show_main_menu(chat_id)



def login_user(chat_id, data):
    login = ''
    password = ''
    try:
        login, password = data.split("\n")
    except ValueError:
        bot.send_message(chat_id=chat_id, text="Invalid data format")
    password = __encrypt_password(password)

    url = f"http://{SERVER_HOST}:{SERVER_PORT}/auth/login?id={chat_id}&login={login}&pswd={password}"
    r = requests.get(url, headers={"Chat_id": str(chat_id)})
    r = r.json()
    response = json.loads(r)
    code = response.get("Status code")

    if code == 200:
        db.hset(chat_id, "Logged_in", '1')

    bot.send_message(chat_id=chat_id, text=response.get("Details"))
    menu_manager.show_main_menu(chat_id)



def logout_user(chat_id):
    url = f"http://{SERVER_HOST}:{SERVER_PORT}/auth/logout?id={chat_id}"
    r = requests.get(url, headers={"Chat_id": str(chat_id)})
    r = r.json()
    response = json.loads(r)
    code = response.get("Status code")

    if code == 200:
        db.hset(chat_id, "Logged_in", '0')

    bot.send_message(chat_id=chat_id, text=response.get("Details"))
    menu_manager.show_main_menu(chat_id)



def write_data(chat_id, data_type, data_bytes):
    url = f"http://{SERVER_HOST}:{SERVER_PORT}/data/write-data?id={chat_id}&type={data_type}&data={data_bytes}"
    r = requests.get(url, headers={"Chat_id": str(chat_id)})
    r = r.json()
    response = json.loads(r)

    bot.send_message(chat_id=chat_id, text=response.get("Details"))
    menu_manager.show_main_menu(chat_id)
